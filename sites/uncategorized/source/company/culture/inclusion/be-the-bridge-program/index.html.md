---
layout: markdown_page
title: "Be The Bridge Program"
description: On this page you will be provided an overview of our Be The Bridge Program.
canonical_path: "/company/culture/inclusion/be-the-bridge-program/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction

On this page you will be provided an overview of our Be The Bridge Program.  This program enables those outside of GitLab to be able to be paired with a internal GitLab team member to learn more about the company, have a mentor relationship, learn more about open source, etc.

We will be working on adding more content on this page. If you are a external person or group and are interested, kindly complete [this interest form](https://docs.google.com/forms/d/19We1NUz5ju0ZPMIkHLivdHf60J9qJN2HizkrTAhFuS4/edit) to be added to our waiting list. We will respond back and hope to pair and accomodate as many of you as possible. Keep in mind we are a small company and will need to continue working on scalable solutions for awesome programs like this. 

## Internal GitLab Process

This process will be managed by the People Operations Specialists, supporting the Diversity, Inclusion and Belonging team. If you are a GitLab team member and would like to volunteer as a mentor to an external person or group for this program, please enter your GitLab information in this [internal form](https://docs.google.com/forms/d/e/1FAIpQLSeI-8c6_sPMtVafuEdEB7Equ8tLQylt5gI4-l2KBR_kjeIIew/viewform).
